#include <iostream>
#include <SDL/SDL.h>

int show(const std::string& caption, const unsigned char* rgba, unsigned w, unsigned h);
int showfile(const char* filename);
